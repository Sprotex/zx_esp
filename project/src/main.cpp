#include "spiffs_server.h"
#include "zx_buffered_file_source.h"
#include "zx_object.h"
#include "zx_vector_source.h"
#include <memory>

#define ZX_EAR 2
#define ZX_MIC 0

spiffs_server server;
zx_object zx_data;
zx_object zx_intro;

std::vector<String> scan_wifi()
{
    std::vector<String> result;
    Serial.print(F("wifi scan start"));
    // WiFi.scanNetworks will return the number of networks found
    int n = WiFi.scanNetworks();
    Serial.println();
    Serial.println(F("wifi scan done"));
    if (n == 0)
        Serial.println(F("no networks found"));
    else
    {
        Serial.print(n);
        result.reserve(n);
        Serial.println(F(" networks found"));
        for (int i = 0; i < n; ++i)
        {
            String wifi_info("");
            wifi_info.concat(i + 1);
            wifi_info.concat(F(": "));
            wifi_info.concat(WiFi.SSID(i));
            wifi_info.concat(F(" ("));
            wifi_info.concat(WiFi.RSSI(i));
            wifi_info.concat(F(")"));
            wifi_info.concat((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? F(" ") : F("*"));
            result.push_back(wifi_info);
        }
    }
    return result;
}

void setup_serial()
{
    Serial.begin(9600);
    while (!Serial)
        ;
}

void setup_zx_objects()
{
    zx_object::set_ear(ZX_EAR);
    zx_object::set_mic(ZX_MIC);
    zx_object::setup();
}

bool connect_to_wifi(const char *ssid, const char *password)
{
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    auto countdown_to_disconnect = 100;
    while (WiFi.status() != WL_CONNECTED && --countdown_to_disconnect)
    {
        delay(100);
    }
    return WiFi.isConnected();
}

String scan_spiffs()
{
    if (SPIFFS.begin())
    {
        Dir dir = SPIFFS.openDir(F("/"));
        while (dir.next())
        {
            auto filename = dir.fileName();
            if (filename.endsWith(F(".tap")) || filename.endsWith(F(".TAP")))
            {
                return filename;
            }
        }
        SPIFFS.end();
    }
    return "";
}

void setup()
{
    setup_serial();
    String selected_tap_name = scan_spiffs();
    bool is_connected = connect_to_wifi("", "");
    auto vector_source = new zx_vector_source();
    zx_intro.set_source(std::unique_ptr<zx_vector_source>(vector_source));
    vector_source->add_print_c_str(F("Config through wifi:"));
    vector_source->add_print_c_str(is_connected ? WiFi.SSID() : spiffs_server::ap_name);
    if (!is_connected)
    {
        vector_source->add_print_c_str(F("with password:"));
        vector_source->add_print_c_str(spiffs_server::ap_password);
    }
    if (selected_tap_name == "")
    {
        vector_source->add_print_c_str(F("No tap found, going to download..."));
    }
    else
    {
        vector_source->add_print_c_str(F("Found tap:"));
        vector_source->add_print_c_str(selected_tap_name.substring(1)); // NOTE(Andy): Substring 1 for the slash at the beginning.
    }
    String localIP = "IP address is " + (is_connected ? WiFi.localIP().toString() : "192.168.1.1");
    vector_source->add_print_c_str(localIP.c_str());
    vector_source->add_load_quotes();
    vector_source->surround_instructions_with_data_block();
    auto vector_size = vector_source->size() - 2;
    vector_source->add_program_header(F("SHOW TAPS"), vector_size, 0);
    setup_zx_objects();
    zx_intro.play_data_tap();
    String downloaded_line = "";
    if (selected_tap_name == "")
    {
        auto client = WiFiClient();
        auto host = "www.kufr.cz";
        auto page = "/jelinek/zx/auto_download/";
        Serial.println(F("Before connect"));
        if (client.connect(host, 80))
        {
            Serial.println(F("After connect OK"));
            client.print(String("GET ") + page + " HTTP/1.1\r\n" +
                         "Host: " + host + "\r\n" +
                         "Connection: close\r\n" +
                         "\r\n");

            auto counter = 0;
            while (client.connected())
            {
                if (client.available())
                {
                    String line = client.readStringUntil('\n');
                    if (counter == 18)
                    {
                        downloaded_line = line;
                        break;
                    }
                    ++counter;
                }
            }
            client.stop();
            Serial.println(downloaded_line);
            downloaded_line = downloaded_line.substring(81);
            auto end_index = downloaded_line.indexOf("\"");
            Serial.println(downloaded_line);
            downloaded_line = downloaded_line.substring(0, end_index);
            Serial.println(downloaded_line);
            wdt_reset();
            auto client2 = WiFiClient();
            if (client2.connect(host, 80))
            {
                counter = 0;
                auto new_page = page + downloaded_line;
                auto request = String("GET ") + page + downloaded_line + " HTTP/1.1\r\n" +
                               "Host: " + host + "\r\n" +
                               "Connection: close\r\n" +
                               "\r\n";
                Serial.println(new_page);
                Serial.println(F("Connected ok 2"));
                Serial.println(request);
                client2.print(request);
                auto content_length = 0;
                while (client2.connected())
                {
                    wdt_reset();
                    if (client2.available())
                    {
                        String line = client2.readStringUntil('\n');
                        ++counter;
                        if (counter == 7)
                        {
                            auto short_line = line.substring(strlen("Content-Length: "));
                            Serial.println(short_line);
                            content_length = short_line.toInt();
                            Serial.println(content_length);
                        }
                        else if (counter > 8)
                        {
                            break;
                        }
                        Serial.print(counter);
                        Serial.print(F(" "));
                        Serial.println(line);
                    }
                }
                uint8_t *tap_buffer = new uint8_t[content_length];
                auto max_read = 1000;
                auto read_count = 0;
                while (read_count * max_read < content_length) {
                    wdt_reset();
                    auto actual_read = (read_count + 1) * max_read > content_length ? content_length - (read_count + 1) * max_read : max_read;
                    Serial.println(actual_read);
                    client2.readBytes(tap_buffer + read_count * max_read, actual_read);
                    ++read_count;
                }
                client2.flush();
                for (int k = 0; k < 10; ++k)
                {
                    for (int i = 0; i < 10; ++i)
                    {
                        Serial.print(tap_buffer[i + k * 10], HEX);
                        Serial.print(" ");
                    }
                    Serial.println();
                }
                if (SPIFFS.begin())
                {
                    selected_tap_name = "/downloaded.tap";
                    auto file = SPIFFS.open("/downloaded.tap", "w");
                    if (!file)
                    {
                        Serial.println(F("File fail"));
                    }
                    file.write(tap_buffer, content_length);
                    file.flush();
                    Serial.print("File size: ");
                    Serial.print(file.size());
                    file.close();
                    Serial.print(" ");
                    Serial.println(file.size());
                } else {
                    Serial.println("SPIFFS fail");
                }
                delete[] tap_buffer;
                client2.stop();
                SPIFFS.end();
            }
        }
        else
        {
            Serial.print("Couldn't connect to ");
            Serial.println(host);
            client.stop();
        }
        Serial.println("After connect");
    }
    if (selected_tap_name != "")
    {
        Serial.println(F("Setup done, play tap..."));
        zx_data.set_source(std::unique_ptr<zx_file_source>(new zx_buffered_file_source(selected_tap_name)));
        zx_data.play_data_tap();
    }
    else
    {
        Serial.println(F("No tap found"));
    }
    zx_intro.dispose();
    zx_data.dispose();
    server.setup(!is_connected);
    Serial.println(F("Setup done"));
}

void loop()
{
    server.loop();
}