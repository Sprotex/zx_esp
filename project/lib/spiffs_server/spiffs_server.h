/*
 * File manager part of this code taken from the FSBrowser sample, part of the ESP8266WebServer library for Arduino environment. Copyright (c) 2015 Hristo Gochkov. All rights reserved.
 * This code also contains excerpts of the examples included in the libraries used, as well as inputs on the esp8266.com forums.
 * Special thanks go to @martinayotte & @igrr for thir insightful help
 */

// NOTE(Andy): Order of includes matter for arduino IDE.
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <FS.h>
#include <vector>

#define SERIAL_DEBUG(data) Serial.print(data)
#define SERIAL_DEBUG_F(data...) Serial.printf(data)
#define SERIAL_DEBUG_LN(data) Serial.println(data)

#define MAX_CACHE_ITEMS 10
#define CACHE_TTL 1 * 60 * 1000 // 1 minute
#define BUFFER_SIZE 5840

class spiffs_server
{
  public:
    static String ap_name;
    static String ap_password;
  private:
    String cachedUrl[MAX_CACHE_ITEMS];        // Urls cached in SPIFFS
    long cacheTime[MAX_CACHE_ITEMS];          // To store time the actual page was last fetched
    String cacheContentType[MAX_CACHE_ITEMS]; // To store the Content-Type of files in cache
    std::vector<String> custom_lines;
    
    ESP8266WebServer server;
    
    //holds the current upload
    File fsUploadFile;
    uint8_t buffer[BUFFER_SIZE];

    String format_bytes(size_t bytes);
    unsigned char h2int(char c);
    String url_decode(String str);
    String url_encode(const char *msg);
    String getContentType(String filename);
    void handle_file_upload();
    void handle_file_delete();
    void handle_file_create();
    void handle_file_list();
    void handle_restart();
    bool handle_file_read(String path);
    bool handle_proxy();
    void handle_root();
    void format_spiffs();
    void uptime();

  public:
    spiffs_server();
    ~spiffs_server();
    void add_line(String line);
    void clear_lines();
    void setup(bool create_access_point = true);
    void loop();
};