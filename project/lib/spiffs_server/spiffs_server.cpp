#include "spiffs_server.h"
#include <functional>

String spiffs_server::ap_name = "ZX_eWIFI_" + WiFi.macAddress();
String spiffs_server::ap_password = "SuperSecure079";

//format bytes
String spiffs_server::format_bytes(size_t bytes)
{
    if (bytes < 1024)
    {
        return String(bytes) + F("B");
    }
    else if (bytes < (1024 * 1024))
    {
        return String(bytes / 1024.0) + F("KB");
    }
    else if (bytes < (1024 * 1024 * 1024))
    {
        return String(bytes / 1024.0 / 1024.0) + F("MB");
    }
    else
    {
        return String(bytes / 1024.0 / 1024.0 / 1024.0) + F("GB");
    }
}

// From https://github.com/zenmanenergy/ESP8266-Arduino-Examples/blob/master/helloWorld_urlencoded/urlencode.ino
unsigned char spiffs_server::h2int(char c)
{
    if (c >= '0' && c <= '9')
    {
        return ((unsigned char)c - '0');
    }
    if (c >= 'a' && c <= 'f')
    {
        return ((unsigned char)c - 'a' + 10);
    }
    if (c >= 'A' && c <= 'F')
    {
        return ((unsigned char)c - 'A' + 10);
    }
    return (0);
}

// From https://github.com/zenmanenergy/ESP8266-Arduino-Examples/blob/master/helloWorld_urlencoded/urlencode.ino
String spiffs_server::url_decode(String str)
{
    String encodedString = F("");
    char c;
    char code0;
    char code1;
    for (unsigned int i = 0; i < str.length(); i++)
    {
        c = str.charAt(i);
        if (c == '+')
        {
            encodedString += ' ';
        }
        else if (c == '%')
        {
            i++;
            code0 = str.charAt(i);
            i++;
            code1 = str.charAt(i);
            c = (h2int(code0) << 4) | h2int(code1);
            encodedString += c;
        }
        else
        {
            encodedString += c;
        }
    }
    return encodedString;
}

// From http://hardwarefun.com/tutorials/url-encoding-in-arduino
String spiffs_server::url_encode(const char *msg)
{
    String hex = F("0123456789abcdef");
    String encodedMsg = F("");

    while (*msg != '\0')
    {
        if (('a' <= *msg && *msg <= 'z') || ('A' <= *msg && *msg <= 'Z') || ('0' <= *msg && *msg <= '9'))
        {
            encodedMsg += *msg;
        }
        else
        {
            encodedMsg += '%';
            encodedMsg += hex[*msg >> 4];
            encodedMsg += hex[*msg & 15];
        }
        msg++;
    }
    return encodedMsg;
}

String spiffs_server::getContentType(String filename)
{
    if (server.hasArg(F("download")))
        return F("application/octet-stream");
    else if (filename.endsWith(F(".htm")) ||
             filename.endsWith(F(".html")))
        return F("text/html");
    else if (filename.endsWith(F(".css")))
        return F("text/css");
    else if (filename.endsWith(F(".js")))
        return F("application/javascript");
    else if (filename.endsWith(F(".png")))
        return F("image/png");
    else if (filename.endsWith(F(".gif")))
        return F("image/gif");
    else if (filename.endsWith(F(".jpg")))
        return F("image/jpeg");
    else if (filename.endsWith(F(".ico")))
        return F("image/x-icon");
    else if (filename.endsWith(F(".xml")))
        return F("text/xml");
    else if (filename.endsWith(F(".pdf")))
        return F("application/x-pdf");
    else if (filename.endsWith(F(".zip")))
        return F("application/x-zip");
    else if (filename.endsWith(F(".gz")))
        return F("application/x-gzip");
    return F("text/plain");
}

void spiffs_server::handle_file_upload()
{
    if (server.uri() != F("/edit"))
        return; //edit
    HTTPUpload &upload = server.upload();
    if (upload.status == UPLOAD_FILE_START)
    {
        String filename = upload.filename;
        if (!filename.startsWith("/"))
        {
            String oldname = filename;
            filename = "/";
            filename += oldname;
        }
        SERIAL_DEBUG(F("handle_file_upload Name: "));
        SERIAL_DEBUG_LN(filename);
        fsUploadFile = SPIFFS.open(filename, "w");
        filename = String();
    }
    else if (upload.status == UPLOAD_FILE_WRITE)
    {
        //SERIAL_DEBUG(F("handle_file_upload Data: ")); SERIAL_DEBUG_LN(upload.currentSize);
        if (fsUploadFile)
            fsUploadFile.write(upload.buf, upload.currentSize);
    }
    else if (upload.status == UPLOAD_FILE_END)
    {
        if (fsUploadFile)
            fsUploadFile.close();
        SERIAL_DEBUG(F("handle_file_upload Size: "));
        SERIAL_DEBUG_LN(upload.totalSize);
    }
    String header = F("HTTP/1.1 303 OK\r\nLocation:");
    header += server.uri();
    header += F("\r\nCache-Control: no-cache\r\n\r\n");
    String redirect = F("<!DOCTYPE HTML>"
                        "<html lang='en'>"
                        "<head>"
                        "<meta charset='UTF-8'>"
                        "<meta name= viewport content=width=device-width, initial-scale=1.0, user-scalable=yes>"
                        "<meta http-equiv='refresh' content='3; URL=/'>"
                        "<style>body {background-color: powderblue;}</style>"
                        "</head>"
                        "\r\n<body>"
                        "<center>"
                        "Automatic refresh in 3 seconds.</center>");
    server.send(200, F("text/html"), redirect);
    // server.sendContent(header);
    // handle_root();
}

void spiffs_server::handle_file_delete()
{
    if (server.args() == 0)
        return handle_root();
    if (server.hasArg(F("Delete")))
    {
        auto filename_to_delete = server.arg(F("Delete"));
        SERIAL_DEBUG_LN(filename_to_delete);
        Dir dir = SPIFFS.openDir("/");
        while (dir.next())
        {
            String path = dir.fileName();
            path.replace(F(" "), F("%20"));
            path.replace(F("ä"), F("%C3%A4"));
            path.replace(F("Ä"), F("%C3%84"));
            path.replace(F("ö"), F("%C3%B6"));
            path.replace(F("Ö"), F("%C3%96"));
            path.replace(F("ü"), F("%C3%BC"));
            path.replace(F("Ü"), F("%C3%9C"));
            path.replace(F("ß"), F("%C3%9F"));
            path.replace(F("€"), F("%E2%82%AC"));
            String comp = path.substring(1);
            SERIAL_DEBUG_LN(comp);
            if (filename_to_delete != comp)
                continue;
            SPIFFS.remove(dir.fileName());
            SERIAL_DEBUG(F("handle_file_delete: "));
            SERIAL_DEBUG_LN(dir.fileName());
            String header = F("HTTP/1.1 303 OK\r\nLocation:");
            header += server.uri();
            header += F("\r\nCache-Control: no-cache\r\n\r\n");
            server.sendContent(header);
            return;
        }
        String redirect;
        redirect += F("<!DOCTYPE HTML>"
                      "<html lang='en'>"
                      "<head>"
                      "<meta charset='UTF-8'>"
                      "<meta name= viewport content=width=device-width, initial-scale=1.0, user-scalable=yes>"
                      "<meta http-equiv='refresh' content='3; URL=");
        redirect += server.uri();
        redirect += F("'>"
                      "<style>body {background-color: powderblue;}</style>"
                      "</head>"
                      "\r\n<body>"
                      "<center>"
                      "<h2>Data not found</h2>"
                      "Automatic refresh in 3 seconds.</center>");
        server.send(200, F("text/html"), redirect);
    }
}

void spiffs_server::handle_file_create()
{
    if (server.args() == 0)
        return server.send(500, F("text/plain"), F("BAD ARGS"));
    String path = server.arg(0);
    SERIAL_DEBUG(F("handle_file_create: "));
    SERIAL_DEBUG_LN(path);
    if (path == "/")
        return server.send(500, F("text/plain"), F("BAD PATH"));
    if (SPIFFS.exists(path))
        return server.send(500, F("text/plain"), F("FILE EXISTS"));
    File file = SPIFFS.open(path, "w");
    if (file)
        file.close();
    else
        return server.send(500, F("text/plain"), F("CREATE FAILED"));
    server.send(200, F("text/plain"), F(""));
    path = String();
}

void spiffs_server::handle_file_list()
{
    if (!server.hasArg(F("dir")))
    {
        server.send(500, F("text/plain"), F("BAD ARGS"));
        return;
    }

    String path = server.arg(F("dir"));
    SERIAL_DEBUG(F("handle_file_list: "));
    SERIAL_DEBUG_LN(path);
    Dir dir = SPIFFS.openDir(path);
    path = String();

    String output = F("[");
    while (dir.next())
    {
        File entry = dir.openFile("r");
        if (output != "[")
            output += ',';
        bool isDir = false;
        output += F("{\'type\':\'");
        output += (isDir) ? F("dir") : F("file");
        output += F("\',\'name\':\'");
        output += String(entry.name()).substring(1);
        output += F("\'}");
        entry.close();
    }

    output += F("]");
    server.send(200, F("text/json"), output);
}

void spiffs_server::handle_restart()
{
    SERIAL_DEBUG_LN(F("Restarting..."));
    ESP.reset();
}

bool spiffs_server::handle_file_read(String path)
{
    SERIAL_DEBUG(F("handle_file_read: "));
    SERIAL_DEBUG_LN(path);
    if (path.endsWith("/"))
        path += F("index.htm");
    String contentType = getContentType(path);
    if (SPIFFS.exists(path))
    {
        File file = SPIFFS.open(path, "r");
        //size_t sent =
        server.streamFile(file, contentType);
        file.close();
        return true;
    }
    return false;
}

bool spiffs_server::handle_proxy()
{
    SERIAL_DEBUG_LN(F("handle_proxy"));

    if (!server.hasArg(F("url")))
    {
        SERIAL_DEBUG_LN(F("url parameter is missing"));
        server.send(200, F("text/plain"), F("Please use http://<this_device>/proxy?url=<encoded_url_without_http>"));
    }
    else
    {
        String realUrl = url_decode(server.arg(F("url")));
        if (realUrl.indexOf("://") == -1)
        {
            // No protocol. Add http://
            String oldUrl = realUrl;
            realUrl = F("http://");
            realUrl += oldUrl;
        }
        if (realUrl.indexOf("/", 7 /* after the http:// */) == -1 && realUrl.indexOf("?") == -1)
        {
            // Only a domain name. Add trailing slash
            // TODO: should handle the case where realUrl contains arguments: http://www.google.com?q=test
            realUrl += "/";
        }
        SERIAL_DEBUG(F("Real url is "));
        SERIAL_DEBUG_LN(realUrl);

        /* Try to find it in cache */
        int cacheIndex = -1;
        long oldestCacheTime = -1;
        int oldestCacheIndex = -1;
        for (int i = 0; i < MAX_CACHE_ITEMS; i++)
        {
            if (cachedUrl[i] == realUrl)
            {
                SERIAL_DEBUG(F("Cache match! This URL is entry #"));
                SERIAL_DEBUG_LN(i);
                cacheIndex = i;
            }
            if (oldestCacheTime == -1 || cacheTime[i] < oldestCacheTime)
            {
                oldestCacheTime = cacheTime[i];
                oldestCacheIndex = i;
            }
        }

        long itemCacheTime;
        if (cacheIndex == -1) /* not found in cache */
        {
            SERIAL_DEBUG(F("Discarding oldest entry #"));
            SERIAL_DEBUG_LN(cacheIndex);
            cacheIndex = oldestCacheIndex; /* recycle the oldest one */
            itemCacheTime = -1;
        }
        else
        {
            itemCacheTime = cacheTime[cacheIndex];
        }

        // Test if we can return the cached copy
        long now = millis();

        if (itemCacheTime == -1    /* no cache */
            || itemCacheTime > now /* millis() wrapped around */
            || now - itemCacheTime > CACHE_TTL /* cache is outdated */)
        {

            SERIAL_DEBUG_LN(F("Fetching from server into cache"));

            // Configure client to retrieve Content-Type header
            HTTPClient http;
            const char *headerkeys[] = {"Content-Type", "Location"};
            size_t headerkeyssize = sizeof(headerkeys) / sizeof(char *);
            //ask to track these headers
            http.collectHeaders(headerkeys, headerkeyssize);

            // Init client
            SERIAL_DEBUG_LN(F("[HTTP] begin..."));
            http.begin(realUrl);

            // Connect to the real server
            SERIAL_DEBUG_LN(F("[HTTP] GET..."));
            int httpCode = http.GET();
            if (httpCode < 0)
            {
                SERIAL_DEBUG(F("[HTTP] GET failed, error: "));
                SERIAL_DEBUG_LN(http.errorToString(httpCode));
                return false;
            }
            SERIAL_DEBUG_F("[HTTP] Status code=%d\n", httpCode);
            if (httpCode != HTTP_CODE_OK)
            {
                SERIAL_DEBUG(F("[HTTP] Location is "));
                SERIAL_DEBUG_LN(http.header("Location"));
                return false;
            }

            // Prepare cache file
            File cache = SPIFFS.open(String(F("/cache/")) + cacheIndex, "w");
            if (!cache)
            {
                SERIAL_DEBUG_LN(F("Could not create cache file"));
            }

            int len = http.getSize();
            SERIAL_DEBUG(F("Response type:"));
            SERIAL_DEBUG(http.header("Content-Type"));
            SERIAL_DEBUG(F(" - Size="));
            SERIAL_DEBUG_LN(len);
            WiFiClient *stream = http.getStreamPtr();
            // read all data from server
            while (http.connected() && (len > 0 || len == -1))
            {
                // get available data size
                size_t size = stream->available();
                if (size)
                {
                    // read up to buffer size
                    int c = stream->readBytes(buffer, ((size > sizeof(buffer)) ? sizeof(buffer) : size));
                    // write it to Serial
                    // SERIAL_DEBUG.write(buffer, c);
                    // write it to cache
                    cache.write(buffer, c);
                    if (len > 0)
                    {
                        len -= c;
                    }
                }
                delay(1);
            }

            cache.close();
            cachedUrl[cacheIndex] = realUrl;
            cacheTime[cacheIndex] = now;
            if (http.hasHeader("Content-Type"))
            {
                cacheContentType[cacheIndex] = http.header("Content-Type");
            }
            else
            {
                // Fallback, probably missing index.html when omitted...
                cacheContentType[cacheIndex] = getContentType(realUrl);
            }
            SERIAL_DEBUG(realUrl);
            SERIAL_DEBUG(F(" (type="));
            SERIAL_DEBUG(cacheContentType[cacheIndex]);
            SERIAL_DEBUG(F(") stored in cache entry #"));
            SERIAL_DEBUG(cacheIndex);
            SERIAL_DEBUG(F(" at "));
            SERIAL_DEBUG(now);
            SERIAL_DEBUG_LN(F("ms"));
        }

        // And return cached file to the browser
        SERIAL_DEBUG_LN(F("Returning contents from cache"));

        // Open cache file for reading
        File cache = SPIFFS.open(String(F("/cache/")) + cacheIndex, "r");
        if (!cache)
        {
            SERIAL_DEBUG_LN(F("Could not open cache file"));
        }
        // And stream it back
        //size_t sent =
        server.streamFile(cache, cacheContentType[cacheIndex]);
        cache.close();
    }

    SERIAL_DEBUG_LN(F("Response sent"));
    return true;
}

void spiffs_server::handle_root()
{ // HTML Start
    FSInfo fs_info;
    SPIFFS.info(fs_info);
    String redirect;
    redirect += F("<!DOCTYPE HTML>"
                  "<html lang='en'>"
                  "<head>"
                  "<meta charset='UTF-8'>"
                  "<meta name= viewport content='width=device-width, initial-scale=1.0,' user-scalable=yes>"
                  "<style type='text/css'>"
                  "<!-- DIV.container { min-height: 10em; display: table-cell; vertical-align: middle }"
                  ".button {height:35px; width:90px; font-size:16px} -->"
                  "body {background-color: powderblue;}"
                  "</style>"
                  "</head>\r\n"
                  "<body>"
                  "<center>"
                  "<h2>ZX ESP 8266 SPIFFS<br>"
                  "ZX transfer, Upload, Download or Delete</h2>"
                  "</center>"
                  "Custom info:"
                  "<p>");
    for (size_t i = 0; i < custom_lines.size(); ++i)
    {
        redirect += custom_lines[i] + F("<br />");
    }
    redirect += F("</p>"
                  "<p>Click file to download: </p>");
    if (!SPIFFS.begin())
        SERIAL_DEBUG_LN(F("SPIFFS failed to mount !\r\n"));
    Dir dir = SPIFFS.openDir("/"); //Lists the files currently in the SPIFFS
    while (dir.next())
    {
        redirect += F("<a href ='");
        redirect += dir.fileName();
        redirect += F("?download='>");
        redirect += F("SPIFFS");
        redirect += dir.fileName();
        redirect += F("</a> ");
        redirect += format_bytes(dir.fileSize()).c_str();
        redirect += F("<br>\r\n");
    }
    redirect += F("<form action='/' method='POST'>"
                  "<input type='text' name='Delete' placeholder='Insert file here' required>\r\n<br>"
                  "<input type='submit' class='button' name='SUBMIT' value='Delete'></form><p>"
                  "<form method='POST' action='/edit' enctype='multipart/form-data' style='height:35px;'>"
                  "<input type='file' name='upload' style='height:35px; font-size:13px;' required>\r\n"
                  "<input type='submit' value='Upload' class='button'></form>"
                  "Upload data<p><p>Datasystem size: ");
    redirect += format_bytes(fs_info.totalBytes).c_str(); //Shows the total size of the file store
    redirect += F("<br>Used: ");
    redirect += format_bytes(fs_info.usedBytes).c_str(); //Shows the space occupied by files
    redirect += F("<p>\r\n"
                  "<p>"
                  "<form action='/file' method='POST'>"
                  "<input name='ButtonName' type='submit' value='Uptime'>"
                  "</form>"
                  "Write system runtime to file\r\n"
                  "<p>"
                  "<form action='/format' method='POST'>"
                  "<input name='ButtonName' type='submit' value='Format SPIFFS'>"
                  "</form>"
                  "This can take up to 30 seconds.</body>"
                  "<form action='/restart' method='GET'>"
                  "<input name='ButtonName' class='button' type='submit' value='Restart'>"
                  "</form>"
                  "</html>\r\n");
    server.send(200, F("text/html"), redirect);
}

void spiffs_server::format_spiffs()
{ //Formats the memory
    SPIFFS.format();
    handle_root();
}

void spiffs_server::uptime()
{                                                          // Create a new file and save the system run time in seconds
    File uptime_file = SPIFFS.open(F("/uptime.txt"), "a"); // If the file exists, the system runtime is written to the next line each time Uptime is clicked
    if (!uptime_file)
        SERIAL_DEBUG_LN(F("file open failed"));
    SERIAL_DEBUG_LN(F("====== Writing to SPIFFS file ========="));
    uptime_file.print(F("Uptime "));
    uptime_file.print(millis() / 1000);
    uptime_file.println(F(" seconds."));
    uptime_file.close();
    handle_root();
}

spiffs_server::spiffs_server() : server(80) {}

spiffs_server::~spiffs_server()
{
    server.close();
}

void spiffs_server::add_line(String line)
{
    custom_lines.push_back(line);
}

void spiffs_server::clear_lines()
{
    custom_lines.clear();
}

void spiffs_server::setup(bool create_access_point)
{
    ap_name.replace(F(":"), F(""));
    IPAddress ip(192, 168, 1, 108);
    IPAddress mask(255, 255, 255, 0);
    if (create_access_point)
    {
        WiFi.mode(WIFI_AP);
        if (!WiFi.softAPConfig(ip, ip, mask))
        {
            SERIAL_DEBUG_LN(F("Fail to set IP!"));
        }
        if (!WiFi.softAP(ap_name.c_str(), ap_password.c_str()))
        {
            SERIAL_DEBUG_LN(F("Couldn\'t make soft AP!"));
        }
    }
    else
    {
        WiFi.mode(WIFI_STA);
    }
    SERIAL_DEBUG_LN(F("Initializing SPIFFS cache..."));
    if (!SPIFFS.begin())
    {
        SERIAL_DEBUG_LN(F("SPIFFS init failed"));
    }
    Dir dir = SPIFFS.openDir(F("/cache"));
    while (dir.next())
    {
        SERIAL_DEBUG(F("Deleting "));
        SERIAL_DEBUG_LN(dir.fileName());
        SPIFFS.remove(dir.fileName());
    }
    for (int i = 0; i < MAX_CACHE_ITEMS; i++)
    {
        cacheTime[i] = 0;
        cachedUrl[i] = F("");
    }
    server.begin();
    SERIAL_DEBUG_LN(F("HTTP server started"));

    // Initialize and start local web server
    server.on("/", HTTP_POST, [this] { handle_file_delete(); });
    server.on("/", [this] { handle_root(); });
    server.on("/format", [this] { format_spiffs(); });
    server.on("/file", [this] { uptime(); });
    server.on("/upload", HTTP_POST,
              [this] { server.send(200, F("text/plain"), F("")); },
              [this] { handle_file_upload(); });

    //list directory
    server.on("/list", HTTP_GET, [this] { handle_file_list(); });
    //list directory
    server.on("/proxy", HTTP_GET, [this] { handle_proxy(); });
    //load editor
    server.on("/edit", HTTP_GET, [this]() {
        if (!handle_file_read(F("/edit.htm")))
            server.send(404, F("text/plain"), F("FileNotFound"));
    });
    //create file
    server.on("/edit", HTTP_PUT, [this] { handle_file_create(); });
    //delete file
    server.on("/edit", HTTP_DELETE, [this] { handle_file_delete(); });
    //first callback is called after the request has ended with all parsed arguments
    //second callback handles file uploads at that location
    server.on("/edit", HTTP_POST, [this]() { server.send(200, F("text/plain"), F("")); }, [this] { handle_file_upload(); });

    server.on("/restart", HTTP_ANY, [this] { handle_restart(); });

    //called when the url is not defined here
    //use it to load content from SPIFFS
    server.onNotFound([this]() {
        if (!handle_file_read(server.uri()))
            server.send(404, F("text/plain"), F("FileNotFound1"));
    });

    //get heap status, analog input value and all GPIO statuses in one json call
    server.on("/all", HTTP_GET, [this]() {
        String json = F("{");
        json += F("\'heap\':");
        json += String(ESP.getFreeHeap());
        json += F(", \'analog\':");
        json += String(analogRead(A0));
        json += F(", \'gpio\':");
        json += String((uint32_t)(((GPI | GPO) & 0xFFFF) | ((GP16I & 0x01) << 16)));
        json += F("}");
        server.send(200, F("text/json"), json);
        json = String();
    });
}

void spiffs_server::loop()
{
    wdt_reset();
    server.handleClient();
}