#include "zx_buffered_file_source.h"

void zx_buffered_file_source::fill_buffer()
{
    buffer.reserve(file.size());
    while (file.position() < file.size()) {
        buffer.push_back(file.read());
    }
    file.close();
}

zx_buffered_file_source::zx_buffered_file_source(String input_filename) : zx_file_source(input_filename)
{
    fill_buffer();
}

size_t zx_buffered_file_source::size() const
{
    return buffer.size();
}

int zx_buffered_file_source::get_next_byte()
{
    auto result = buffer[current_position];
    current_position++;
    if (current_position > size()) {
        //Serial.println("OVERFLOW");
        return 0;
    }
    return result;
}

void zx_buffered_file_source::set_start_position(size_t position)
{
    current_position = position;
}
