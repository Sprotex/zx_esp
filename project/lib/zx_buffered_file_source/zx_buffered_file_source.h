#ifndef __ZX_BUFFERED_FILE_SOURCE_H_19878198289648649217910972723051025093285682621830__
#define __ZX_BUFFERED_FILE_SOURCE_H_19878198289648649217910972723051025093285682621830__

#include "zx_file_source.h"
#include <vector>

class zx_buffered_file_source : public zx_file_source
{
  protected:
    std::vector<byte> buffer;
    size_t current_position;

    void fill_buffer();

  public:
    zx_buffered_file_source(String input_filename);
    virtual size_t size() const;
    virtual int get_next_byte();
    virtual void set_start_position(size_t position);
};

#endif /* __ZX_BUFFERED_FILE_SOURCE_H_19878198289648649217910972723051025093285682621830__ */
