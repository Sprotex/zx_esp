#include "zx_vector_source.h"
#include "zx_object.h"

unsigned char zx_header_chars[] = // zmenili jsme to na originalni TAP format: tj. pred tim maj bejt dva bajty velikosti (nejdriv low byte, )
    {
        0x00,                                                       // flagheader
        0x00,                                                       // type program
        'm', 'A', 'T', 'A', 'R', 'I', 'n', '0', '.', '1',
        0xde, 0x01,                                                 // length                                 pos -7 -6
        0x00, 0x00,                                                 // start from line 0                      pos -4 -5
        0xde, 0x01,                                                 // param2 = length                        pos -3 -2
        0x00                                                        // space for checksum - calculated later  pos -1
};

unsigned char zx_data_chars[] =
    {
        0xff,                                                 // flagblock .. programfile in Basic
        0x00, 0x01,                                           // line basic num 01
        0x09, 0x00,                                           // line basic length
        0xe7, 0x30, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, // 1 BORDER 0
        0x00, 0x02,                                           // line  basic 02
        0x09, 0x00,                                           // line basic length
        0xd9, 0x37, 0x0e, 0x00, 0x00, 0x07, 0x00, 0x00, 0x0d, // 2 INK 7
        0x00, 0x03,                                           // line 03
        0x09, 0x00,
        0xda, 0x30, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, // 3 PAPER 0
        0x00, 0x04,                                           // line 04
        0x02, 0x00,
        0xfb, 0x0d, // CLS
        0x00, 0x0a, // line 10
        0x12, 0x00,
        0xf5, 0xac, 0x30, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x2c, 0x30, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, // PRINT AT 0,0
        0x63 // checksum
};

zx_object zx_header(zx_header_chars, 19);
zx_object zx_data;
zx_vector_source zx_vector;
zx_vector_source zx_header_vector;


void loop_zx()
{
    Serial.print("Start of ZX transfer ");
    /*
    unsigned char data_to_add[] = {
        0x27, 0x0F,       // Line num 9999
        0x03, 0x00,       // line length
        0xef, 0x22, 0x22, // load ""
        0x0d,             // cr
    };
    */
    //size_t data_to_add_size = sizeof(data_to_add) / sizeof(data_to_add[0]);
    //zx_data.add_data(data_to_add, data_to_add_size);
    zx_data.add_print_c_str("Manufactured in Podoli 2018", 9000);
    zx_data.add_load_quotes(9999);
    zx_header_vector.store_length_of(zx_data);
    zx_header_vector.calculate_checksum();
    zx_vector.calculate_checksum();
    zx_header.play_data();
    zx_data.play_data();
    Serial.println();
    Serial.println("Stop");
}