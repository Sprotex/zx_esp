#include "custom_symbols.h"
#include "zx_object.h"

int zx_object::ear = 0;
int zx_object::mic = 0;
int zx_object::debug_ptr = 1;
int zx_object::last_micros[DEBUG_LENGTH] =
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int zx_object::last_periods[DEBUG_LENGTH] =
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

void zx_object::play_zx_up(unsigned int t)
{
    digitalWrite(ear, HIGH);
    delayMicroseconds(t / CORRECTION);
}

void zx_object::play_zx_down(unsigned int t)
{
    digitalWrite(ear, LOW);
    delayMicroseconds(t / CORRECTION);
}

void zx_object::play_zx_tone(unsigned int t)
{
    int current_micros = micros();
    int last_micro = last_micros[debug_ptr - 1];
    int last_period = last_periods[debug_ptr - 1];
    int current_period = current_micros - last_micro;
    if (debug_ptr >= DEBUG_LENGTH)
    {
        debug_ptr = 1;
        last_periods[0] = current_period;
        last_micros[0] = current_micros;
    }
    if (last_period != 1253 && last_period != 1252)
    {
        last_periods[debug_ptr] = current_period;
        last_micros[debug_ptr] = current_micros;
        debug_ptr++;
    }
    play_zx_up(t);
    play_zx_down(t);
    wdt_reset(); // for convenience, if called in loop it is necessary
}

void zx_object::play_bit(int b)
{
    if (b == 0)
    {
        play_zx_tone(855);
    }
    else
    {
        play_zx_tone(1710);
    }
}

// PLAY pilot TONE  ==============================================
// Pulse the speaker to play a tone for a particular duration
void zx_object::play_pilot()
{
    int tone_length = 1000 * 2;
    for (int j = 0; j < tone_length; j++)
        play_zx_tone(2168);
}

void zx_object::play_sync()
{
    play_zx_up(667);
    play_zx_down(735);
    wdt_reset();
}

void zx_object::set_current_line(size_t next_line)
{
    current_line = next_line;
}

size_t zx_object::size() const
{
    return source->size();
}

void zx_object::set_ear(int zx_ear)
{
    zx_object::ear = zx_ear;
}

void zx_object::set_mic(int zx_mic)
{
    zx_object::mic = zx_mic;
}

void zx_object::setup()
{
    pinMode(ear, OUTPUT);
    pinMode(mic, INPUT_PULLUP);
}

zx_object::zx_object() : source(nullptr), current_line(1) {}

void zx_object::set_source(std::unique_ptr<zx_source> new_source)
{
    source = std::move(new_source);
}

void zx_object::play_data()
{
    source->set_start_position(0);
    play_data(size() + 1);
}

void zx_object::play_byte(byte current)
{
    for (byte j = 0; j < 8; j++)
    {
        play_bit(current & 128);
        current <<= 1; // shift the bits in data left by one bit
    }
}

void zx_object::play_data(size_t count)
{
    play_pilot();
    play_sync();
    last_micros[0] = micros();
    last_periods[0] = 0;
    for (unsigned int i = 0; i < count; i++)
    {
        byte current = source->get_next_byte(); // print ASCII decimal values
        play_byte(current);
    }
    last_periods[0] = last_periods[debug_ptr - 1];
    last_micros[0] = last_micros[debug_ptr - 1];
    debug_ptr = 1;
}

void zx_object::play_data_tap()
{
    // to test:
    // current_size - 1 : checksum error
    //
    size_t current_idx = 0;
    size_t current_size = size() + 1;
    Serial.print(F("zx_obj size: "));
    Serial.println(current_size);
    source->set_start_position(0);
    while (current_idx < current_size)
    {
        byte low_byte = source->get_next_byte();
        byte high_byte = source->get_next_byte();
        size_t current_beep_length = low_byte + (high_byte << 8);
        Serial.print(F("idx/len:"));
        Serial.print(current_idx);
        Serial.print(F("/"));
        Serial.println(current_beep_length);
        if (current_idx >= current_size - 1)
        {
            current_beep_length = 0;
        }
        play_data(current_beep_length);
        current_idx += 2 + current_beep_length;
        //play_data(current_beep_length);
        //current_idx += 2 + current_beep_length;
    }
    Serial.println("tap playing done");
}

void zx_object::print_debug(Print &printable)
{
    const size_t newliner = 16;
    printable.print("Size: ");
    printable.println(size());
    source->set_start_position(0);
    for (size_t i = 0; i < size(); ++i)
    {
        byte current = source->get_next_byte();
        print_hex(current, Serial);
        printable.print(' ');
        if (i % newliner == newliner - 1)
        {
            wdt_reset();
            printable.println();
        }
    }
}

void zx_object::print(Print &printable)
{
    printable.print("Size: ");
    printable.println(size());
    source->set_start_position(0);
    size_t current_idx = 0;
    while (current_idx < size())
    {
        byte low_byte = source->get_next_byte();
        byte high_byte = source->get_next_byte();
        print_hex(low_byte, Serial);
        printable.print(" ");
        print_hex(high_byte, Serial);
        printable.println();
        size_t current_beep_length = low_byte + (high_byte << 8);
        current_idx += 2 + current_beep_length;
        for (size_t i = 0; i < current_beep_length; ++i)
        {
            byte to_print = source->get_next_byte();
            print_hex(to_print, Serial);
            printable.print(" ");
            wdt_reset();
            current_idx++;
        }
        printable.println();
    }
}

void zx_object::dispose() {
    source.reset(nullptr);
}