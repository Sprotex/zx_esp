#ifndef __ZX_OBJECT_H_128834895909674636563785698798984578345623562356785896__
#define __ZX_OBJECT_H_128834895909674636563785698798984578345623562356785896__

/*
  ID : 11 - Turbo Loading Data Block
  This block is very similar to the normal TAP block but with some additional info on the timings and other important differences. The same tape encoding is used as for the standard speed data block. If a block should use some non-standard sync or pilot tones (i.e. all sorts of protection schemes) then use the next three blocks to describe it.
  00 2  Length of PILOT pulse                                            [2168]
  02 2  Length of SYNC First pulse                                        [667]
  04 2  Length of SYNC Second pulse                                       [735]
  06 2  Length of ZERO bit pulse                                          [855]
  08 2  Length of ONE bit pulse                                          [1710]
  0A 2  Length of PILOT tone (in PILOT pulses)         [8064 Header, 3220 Data]
  0C 1  Used bits in last byte (other bits should be 0)                     [8]
      i.e. if this is 6 then the bits (x) used in last byte are: xxxxxx00
  0D 2  Pause After this block in milliseconds (ms)                      [1000]
  0F 3  Length of following data
  12 x  Data; format is as for TAP (MSb first)
  - Length: [0F,10,11]+18
*/
#include <ESP8266WiFi.h>
#include <vector>
#include <memory>
#include <Arduino.h>
#include "zx_source.h"

#define DEBUG_LENGTH 100

class zx_object
{
  private:
    //const float CORRECTION = 3.19; //orig
    const float CORRECTION = 3.7;
    void play_zx_up(unsigned int t);
    void play_zx_down(unsigned int t);
    void play_zx_tone(unsigned int t);
    void play_bit(int b);
    void play_byte(byte b);
    void play_pilot();
    void play_sync();
    static int ear;
    static int mic;
    static int debug_ptr;
    static int last_micros[DEBUG_LENGTH];
    static int last_periods[DEBUG_LENGTH];
    std::unique_ptr<zx_source> source;
    size_t current_line;

  public:
    static void set_ear(int zx_ear);
    static void set_mic(int zx_mic);
    static void setup();
    zx_object();
    void set_source(std::unique_ptr<zx_source> new_source);
    void clear_source();
    void set_current_line(size_t next_line);
    size_t size() const;
    void play_data();
    void play_data(size_t count);
    void play_data_tap();
    unsigned char &operator[](int i);
    void print_debug(Print &printable);
    void print(Print &printable);
    void dispose();
};

#endif /* __ZX_OBJECT_H_128834895909674636563785698798984578345623562356785896__ */