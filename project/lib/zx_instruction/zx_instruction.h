#ifndef __ZX_INSTRUCTION_H_129856189896123069068194380213869253685129__
#define __ZX_INSTRUCTION_H_129856189896123069068194380213869253685129__

#include <vector>
#include <cstddef>

class zx_instruction {
    public:
      virtual size_t size() = 0;
      virtual std::vector<unsigned char> raw_data() = 0;
};

#endif