#include "zx_file_source.h"

zx_file_source::zx_file_source(String input_filename)
{
    if (input_filename != "")
        open(input_filename);
}

zx_file_source::~zx_file_source()
{
    close();
}

size_t zx_file_source::size() const
{
    if (file)
    {
        return file.size();
    }
    return 0;
}

int zx_file_source::get_next_byte()
{
    int result = file.read();
    if (result == -1) {
        Serial.println(F("EOF"));
        return 0;
    }
    return result;
}

void zx_file_source::set_start_position(size_t position)
{
    file.seek(position, SeekMode::SeekSet);
}

void zx_file_source::open(String filename)
{
    if (!SPIFFS.begin())
    {
        Serial.println("Error openning SPIFFS");
    }
    else
    {
        close();
        file = SPIFFS.open(filename, "r");
        if (!file)
        {
            Serial.println("Error openning file");
        } else {
            Serial.print("Opened file (");
            Serial.print(size());
            Serial.println(")");
            Serial.print("Peek: ");
            Serial.println(file.peek());
        }
    }
}

void zx_file_source::close()
{
    if (file) {
        file.close();
        Serial.println("close");
    }
}