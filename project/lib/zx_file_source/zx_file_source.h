#ifndef __ZX_FILE_SOURCE_H_461235421312356152356468756123542031__
#define __ZX_FILE_SOURCE_H_461235421312356152356468756123542031__

#include "zx_source.h"
#include <cstddef>
#include <Arduino.h>
#include <FS.h>

class zx_file_source : public zx_source
{
  protected:
    File file;

  public:
    zx_file_source(String input_filename);
    virtual ~zx_file_source();
    virtual size_t size() const;
    virtual int get_next_byte();
    virtual void set_start_position(size_t position);
    void open(String filename);
    void close();
};

#endif /* __ZX_FILE_SOURCE_H_461235421312356152356468756123542031__ */
