#ifndef __ZX_SOURCE_H_1298149889563290877671523765123178387__
#define __ZX_SOURCE_H_1298149889563290877671523765123178387__

#include <Arduino.h>
#include <cstddef>

class zx_source
{
  public:
    virtual ~zx_source() = 0;
    virtual size_t size() const = 0;
    virtual int get_next_byte() = 0;
    virtual void set_start_position(size_t position) = 0;
};

#endif /*__ZX_SOURCE_H_1298149889563290877671523765123178387__*/