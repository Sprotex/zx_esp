#ifndef __ZX_VECTOR_SOURCE_H_12891589980689754625623456657783427698493999378568__
#define __ZX_VECTOR_SOURCE_H_12891589980689754625623456657783427698493999378568__

#include "zx_source.h"
#include "zx_object.h"
#include <vector>

#define INIT_SIZE 256

class zx_vector_source : public zx_source
{
  private:
    std::vector<unsigned char> data;
    size_t current_position;
    size_t current_input_position;
    void add_string(String text, size_t line_number = 0);

  public:
    zx_vector_source(size_t init_capacity = INIT_SIZE);
    virtual ~zx_vector_source();
    virtual size_t size() const;
    virtual int get_next_byte();
    virtual void set_start_position(size_t position);
    void set_input_position(size_t position);
    void clear();
    void set_line_number_to_data(std::vector<unsigned char>& input_data, size_t line_number);
    void add_byte(byte input_byte);
    void add_data(const std::vector<unsigned char>& input_data);
    void add_load_quotes(size_t line_number = 0);
    void add_print(String text, size_t line_number = 0);
    void add_print_c_str(String text, size_t line_number = 0);
    void store_length_of(zx_vector_source &data_object);
    int calculate_checksum();
    int calculate_checksum(const std::vector<unsigned char>& input_data);
    void add_program_header(String name, size_t length, size_t autostart_line, bool at_beginning = true);
    void surround_instructions_with_data_block();
    unsigned char &operator[](int i);
};

#endif /* __ZX_VECTOR_SOURCE_H_12891589980689754625623456657783427698493999378568__ */