#include "custom_symbols.h"
#include "zx_vector_source.h"

zx_vector_source::zx_vector_source(size_t init_capacity) : current_position(0), current_input_position(0)
{
    data.reserve(init_capacity);
}

zx_vector_source::~zx_vector_source() {}

size_t zx_vector_source::size() const
{
    return data.size();
}

int zx_vector_source::get_next_byte()
{
    return data[current_position++];
}

void zx_vector_source::set_start_position(size_t position)
{
    current_position = position;
}

void zx_vector_source::set_input_position(size_t position)
{
    current_input_position = position;
}

void zx_vector_source::clear()
{
    data.clear();
}

void zx_vector_source::set_line_number_to_data(std::vector<unsigned char> &input_data, size_t line_number)
{
    if (line_number == 0)
    {
        line_number = current_input_position++;
    }
    byte high = highByte(line_number);
    byte low = lowByte(line_number);
    Serial.print("Current pos:");
    Serial.println(line_number);
    input_data[0] = high;
    input_data[1] = low;
}

void zx_vector_source::add_byte(byte input_byte)
{
    data.push_back(input_byte);
}

void zx_vector_source::add_data(const std::vector<unsigned char> &input_data)
{
    data.insert(data.end(), input_data.begin(), input_data.end());
}

void zx_vector_source::add_load_quotes(size_t line_number)
{
    std::vector<unsigned char> input_data = {
        0x00, 0x00, // space for line number
        0x03, 0x00, // instruction size in bytes
        Symbol::ZX_LOAD, Symbol::QUOTE, Symbol::QUOTE,
        Symbol::CARRIAGE_RETURN};
    set_line_number_to_data(input_data, line_number);
    add_data(input_data);
}

void zx_vector_source::add_print(String text, size_t line_number)
{
    size_t text_length = text.length();
    size_t aux_length = 2 + 2 + 1 + 1; // space for line number + space for instruction size number + instruction code + carriage return
    size_t input_data_size = text_length + aux_length;
    std::vector<unsigned char> input_data(input_data_size);
    set_line_number_to_data(input_data, line_number);
    input_data[2] = lowByte(text_length + 2);
    input_data[3] = highByte(text_length + 2);
    input_data[4] = Symbol::ZX_PRINT;
    size_t position_to_copy = aux_length - 1; // not the carriage return that goes afterwards
    for (size_t i = 0; i < text_length; ++i)
    {
        input_data[position_to_copy + i] = text[i];
    }
    input_data[input_data_size - 1] = Symbol::CARRIAGE_RETURN;
    add_data(input_data);
}

void zx_vector_source::store_length_of(zx_vector_source &data_object)
{
    int data_size = data_object.size();
    int inner_data_size = data_size - 2;
    // store calculated length of basic program to header
    (*this)[-2] = highByte(inner_data_size);
    (*this)[-3] = lowByte(inner_data_size);
    (*this)[-6] = highByte(inner_data_size);
    (*this)[-7] = lowByte(inner_data_size);
}

int zx_vector_source::calculate_checksum()
{
    return calculate_checksum(data);
}

int zx_vector_source::calculate_checksum(const std::vector<unsigned char> &input_data)
{
    int checksum = 0;
    size_t data_size = input_data.size();
    for (unsigned int i = 0; i < data_size - 1; ++i)
    {
        checksum ^= input_data[i];
    }
    return checksum;
}

void zx_vector_source::add_program_header(String name, size_t length, size_t autostart_line, bool at_beginning)
{
    byte tap_data_length = 19;
    std::vector<unsigned char> new_data = {
        tap_data_length, 0, // 2
        0, 0, // 2
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', // 10
        lowByte(length), highByte(length), // 2
        lowByte(autostart_line), highByte(autostart_line), // 2
        lowByte(32768), highByte(32768), // 2
        0}; // 1
    int name_length = name.length() > 10 ? 10 : name.length();
    for (int i = 0; i < name_length; ++i)
    {
        new_data[i + 4] = name[i];
    }
    std::vector<unsigned char> checksum_data(new_data.begin() + 2, new_data.end());
    new_data[new_data.size() - 1] = calculate_checksum(checksum_data);
    data.insert(at_beginning ? data.begin() : data.end(), new_data.begin(), new_data.end());
}

void zx_vector_source::surround_instructions_with_data_block()
{
    data.insert(data.begin(), 0xFF);
    size_t checksum_position = size();
    data.push_back(0);
    int checksum = calculate_checksum();
    data[checksum_position] = checksum;
    size_t current_size = size();
    data.insert(data.begin(), highByte(current_size));
    data.insert(data.begin(), lowByte(current_size));
}

void zx_vector_source::add_print_c_str(String text, size_t line_number)
{
    String quote = String((char)Symbol::QUOTE);
    String quoted_text = quote + text + quote;
    Serial.println(quoted_text);
    add_print(quoted_text, line_number);
}

unsigned char &zx_vector_source::operator[](int i)
{
    if (i < 0)
    {
        i = size() + i;
    }
    while (i >= (int)size())
    {
        i -= size();
    }
    return data[i];
}
