#ifndef __CUSTOM_SYMBOLS_H_18189319860917611561235717615593689980732408476345872330278__
#define __CUSTOM_SYMBOLS_H_18189319860917611561235717615593689980732408476345872330278__

#include <Arduino.h>
typedef unsigned char byte;

class Symbol
{
  public:
    static const byte QUOTE = 0x22;           // "
    static const byte CARRIAGE_RETURN = 0x0D; // \r
    static const byte ZX_RND = 0xA5;
    static const byte ZX_INKEY$ = 0xA6;
    static const byte ZX_PI = 0xA7;
    static const byte ZX_FN = 0xA8;
    static const byte ZX_POINT = 0xA9;
    static const byte ZX_SCREEN$ = 0xAA;
    static const byte ZX_ATTR = 0xAB;
    static const byte ZX_AT = 0xAC;
    static const byte ZX_TAB = 0xAD;
    static const byte ZX_VAL$ = 0xAE;
    static const byte ZX_CODE = 0xAF;
    static const byte ZX_VAL = 0xB0;
    static const byte ZX_LEN = 0xB1;
    static const byte ZX_SIN = 0xB2;
    static const byte ZX_COS = 0xB3;
    static const byte ZX_TAN = 0xB4;
    static const byte ZX_ASN = 0xB5;
    static const byte ZX_ACS = 0xB6;
    static const byte ZX_ATN = 0xB7;
    static const byte ZX_LN = 0xB8;
    static const byte ZX_EXP = 0xB9;
    static const byte ZX_INT = 0xBA;
    static const byte ZX_SOR = 0xBB;
    static const byte ZX_SGN = 0xBC;
    static const byte ZX_ABS = 0xBD;
    static const byte ZX_PEEK = 0xBE;
    static const byte ZX_IN = 0xBF;
    static const byte ZX_USR = 0xC0;
    static const byte ZX_STR$ = 0xC1;
    static const byte ZX_CHR$ = 0xC2;
    static const byte ZX_NOT = 0xC3;
    static const byte ZX_BIN = 0xC4;
    static const byte ZX_OR = 0xC5;
    static const byte ZX_AND = 0xC6;
    static const byte ZX_LESS_THAN = 0xC7;
    static const byte ZX_GREATER_THAN = 0xC8;
    static const byte ZX_NOT_EQUAL = 0xC9;
    static const byte ZX_LINE = 0xCA;
    static const byte ZX_THEN = 0xCB;
    static const byte ZX_TO = 0xCC;
    static const byte ZX_STEP = 0xCD;
    static const byte ZX_DEF_FN = 0xCE;
    static const byte ZX_CAT = 0xCF;
    static const byte ZX_FORMAT = 0xD0;
    static const byte ZX_MOVE = 0xD1;
    static const byte ZX_ERASE = 0xD2;
    static const byte ZX_OPEN_HASH = 0xD3;
    static const byte ZX_CLOSE_HASH = 0xD4;
    static const byte ZX_MERGE = 0xD5;
    static const byte ZX_VERIFY = 0xD6;
    static const byte ZX_BEEP = 0xD7;
    static const byte ZX_CIRCLE = 0xD8;
    static const byte ZX_INK = 0xD9;
    static const byte ZX_PAPER = 0xDA;
    static const byte ZX_FLASH = 0xDB;
    static const byte ZX_BRIGHT = 0xDC;
    static const byte ZX_INVERSE = 0xDD;
    static const byte ZX_OVER = 0xDE;
    static const byte ZX_OUT = 0xDF;
    static const byte ZX_LPRINT = 0xE0;
    static const byte ZX_LLIST = 0xE1;
    static const byte ZX_STOP = 0xE2;
    static const byte ZX_READ = 0xE3;
    static const byte ZX_DATA = 0xE4;
    static const byte ZX_RESTORE = 0xE5;
    static const byte ZX_NEW = 0xE6;
    static const byte ZX_BORDER = 0xE7;
    static const byte ZX_CONTINUE = 0xE8;
    static const byte ZX_DIM = 0xE9;
    static const byte ZX_REM = 0xEA;
    static const byte ZX_FOR = 0xEB;
    static const byte ZX_GO_TO = 0xEC;
    static const byte ZX_GO_SUB = 0xED;
    static const byte ZX_INPUT = 0xEE;
    static const byte ZX_LOAD = 0xEF;
    static const byte ZX_LIST = 0xF0;
    static const byte ZX_LET = 0xFl;
    static const byte ZX_PAUSE = 0xF2;
    static const byte ZX_NEXT = 0xF3;
    static const byte ZX_POKE = 0xF4;
    static const byte ZX_PRINT = 0xF5;
    static const byte ZX_PLOT = 0xF6;
    static const byte ZX_RUN = 0xF7;
    static const byte ZX_SAVE = 0xF8;
    static const byte ZX_RANDOMIZE = 0xF9;
    static const byte ZX_IF = 0xFA;
    static const byte ZX_CLS = 0xFB;
    static const byte ZX_DRAW = 0xFC;
    static const byte ZX_CLEAR = 0xFD;
    static const byte ZX_RETURN = 0xFE;
    static const byte ZX_COPY = 0xFF;
    static const byte ZX_DELETE = 0x0C;
    static const byte ZX_ENTER = 0x0D;
    static const byte ZX_INK_control = 0x10;
    static const byte ZX_PAPER_control = 0x11;
    static const byte ZX_FLASH_control = 0x12;
    static const byte ZX_BRIGHT_control = 0x13;
    static const byte ZX_INVERSE_control = 0x14;
    static const byte ZX_OVER_control = 0x15;
    static const byte ZX_AT_control = 0x16;
    static const byte ZX_TAB_control = 0x17;
};

template <class T>
void print_hex(const T &to_print, Print &printable)
{
    if (to_print < 16)
    {
        printable.print(0);
    }
    printable.print(to_print, HEX);
}

#endif /* __CUSTOM_SYMBOLS_H_18189319860917611561235717615593689980732408476345872330278__ */